var clientes;

function getClientes() {
  var url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200){
      //console.log(request.responseText);
      clientes = request.responseText;
      procesarClientes();
    }
  }
  request.open("GET", url, true);
  request.send();

}

function procesarClientes(){
  var JSONProductos = JSON.parse(clientes);
  //alert(JSONProductos.value[0].ProductName);
  var divTabla = document.getElementById("tablaProductos");
  var tabla = document.createElement("table");
  var tbody = document.createElement("tbody");

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  for (var i = 0; i < JSONProductos.value.length; i++) {
    var nuevaFila = document.createElement("tr");
    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONProductos.value[i].ContactName;
    var columnaCiudad = document.createElement("td");
    columnaCiudad.innerText = JSONProductos.value[i].City;
    var img = document.createElement("img");

    var bandera = JSONProductos.value[i].Country;
    if(bandera != "UK"){
      img.setAttribute('src', 'https://www.countries-ofthe-world.com/flags-normal/flag-of-'+bandera+'.png');
    }else{
       img.setAttribute('src', 'https://www.countries-ofthe-world.com/flags-normal/flag-of-United-Kingdom.png');
    }

    img.setAttribute('width', '70px');
    img.setAttribute('height', '42px');

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaCiudad);
    nuevaFila.appendChild(img);

    tbody.appendChild(nuevaFila);
  }
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);


}
